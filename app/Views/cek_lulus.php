<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $title ?></title>
	<meta name="description" content="The small framework with powerful features">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<!-- STYLES -->

	<style {csp-style-nonce}>
		* {
			transition: background-color 300ms ease, color 300ms ease;
		}
		*:focus {
			background-color: rgba(221, 72, 20, .2);
			outline: none;
		}
		html, body {
			color: rgba(33, 37, 41, 1);
			font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";
			font-size: 16px;
			margin: 0;
			padding: 0;
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
			text-rendering: optimizeLegibility;
		}
		header {
			background-color: rgba(247, 248, 249, 1);
			padding: .4rem 0 0;
		}
		.menu {
			padding: .4rem 2rem;
		}
		header ul {
			border-bottom: 1px solid rgba(242, 242, 242, 1);
			list-style-type: none;
			margin: 0;
			overflow: hidden;
			padding: 0;
			text-align: right;
		}
		header li {
			display: inline-block;
		}
		header li a {
			border-radius: 5px;
			color: rgba(0, 0, 0, .5);
			display: block;
			height: 44px;
			text-decoration: none;
		}
		header li.menu-item a {
			border-radius: 5px;
			margin: 5px 0;
			height: 38px;
			line-height: 36px;
			padding: .4rem .65rem;
			text-align: center;
		}
		header li.menu-item a:hover,
		header li.menu-item a:focus {
			background-color: rgba(221, 72, 20, .2);
			color: rgba(221, 72, 20, 1);
		}
		header .logo {
			float: left;
			height: 44px;
			padding: .4rem .5rem;
		}
		header .menu-toggle {
			display: none;
			float: right;
			font-size: 2rem;
			font-weight: bold;
		}
		header .menu-toggle button {
			background-color: rgba(221, 72, 20, .6);
			border: none;
			border-radius: 3px;
			color: rgba(255, 255, 255, 1);
			cursor: pointer;
			font: inherit;
			font-size: 1.3rem;
			height: 36px;
			padding: 0;
			margin: 11px 0;
			overflow: visible;
			width: 40px;
		}
		header .menu-toggle button:hover,
		header .menu-toggle button:focus {
			background-color: rgba(221, 72, 20, .8);
			color: rgba(255, 255, 255, .8);
		}
		header .heroe {
			margin: 0 auto;
			max-width: 1100px;
			padding: 1rem 1.75rem 1.75rem 1.75rem;
		}
		header .heroe h1 {
			font-size: 2.5rem;
			font-weight: 500;
		}
		header .heroe h2 {
			font-size: 1.5rem;
			font-weight: 300;
		}
		section {
			text-align: center;
			margin: 0 auto;
			max-width: 1100px;
			padding: 2.5rem 1.75rem 3.5rem 1.75rem;
		}
		section h1 {
			margin-bottom: 2.5rem;
		}
		section h2 {
			font-size: 120%;
			line-height: 2.5rem;
			padding-top: 1.5rem;
		}
		section pre {
			background-color: rgba(247, 248, 249, 1);
			border: 1px solid rgba(242, 242, 242, 1);
			display: block;
			font-size: .9rem;
			margin: 2rem 0;
			padding: 1rem 1.5rem;
			white-space: pre-wrap;
			word-break: break-all;
		}
		section code {
			display: block;
		}
		section a {
			color: rgba(221, 72, 20, 1);
		}
		section svg {
			margin-bottom: -5px;
			margin-right: 5px;
			width: 25px;
		}
		.further {
			background-color: rgba(247, 248, 249, 1);
			border-bottom: 1px solid rgba(242, 242, 242, 1);
			border-top: 1px solid rgba(242, 242, 242, 1);
		}
		.further h2:first-of-type {
			padding-top: 0;
		}
		footer {
			background-color: rgba(221, 72, 20, .8);
			text-align: center;
		}
		footer .environment {
			color: rgba(255, 255, 255, 1);
			padding: 2rem 1.75rem;
		}
		footer .copyrights {
			background-color: rgba(62, 62, 62, 1);
			color: rgba(200, 200, 200, 1);
			padding: .25rem 1.75rem;
		}
		@media (max-width: 559px) {
			header ul {
				padding: 0;
			}
			header .menu-toggle {
				padding: 0 1rem;
			}
			header .menu-item {
				background-color: rgba(244, 245, 246, 1);
				border-top: 1px solid rgba(242, 242, 242, 1);
				margin: 0 15px;
				width: calc(100% - 30px);
			}
			header .menu-toggle {
				display: block;
			}
			header .hidden {
				display: none;
			}
			header li.menu-item a {
				background-color: rgba(221, 72, 20, .1);
			}
			header li.menu-item a:hover,
			header li.menu-item a:focus {
				background-color: rgba(221, 72, 20, .7);
				color: rgba(255, 255, 255, .8);
			}
		}
	</style>
</head>
<body>

	<!-- HEADER: MENU + HEROE SECTION -->
	<header>

		<div class="menu">
			<ul>
				<li class="logo"><b><font size="5">SIKELUS V1</font></b></a>
				</li>
				<li class="menu-toggle">
					<button onclick="toggleMenu();">&#9776;</button>
				</li>
				<li class="menu-item hidden"><a href="<?php echo base_url() ?>">Home</a></li>
				<li class="menu-item hidden"><a href="<?php echo base_url('Home/validasi') ?>">Validasi</a></li>
			</li>
		</ul>
	</div>

	<div class="heroe">

		<h1>Selamat datang di demo SIKELUS V.1</h1>

		<h2>Sistem Informasi Kelulusan Siswa by SukabumiWebSolution.com</h2>
	</div>

</header>

<!-- CONTENT -->
<div class="further">

	<section>

		<h3>Siswa dibawah ini :</h3>
		<table align="center" class="table" style="width: 50%;">
			<tr>
				<td align="left">Nama</td>
				<td align="left">: <?php echo $data_lulus->nama ?></td>
			</tr>
			<tr>
				<td align="left">Tempat, Tanggal Lahir</td>
				<td align="left">: <?php echo $data_lulus->tempat_lahir.", ".$data_lulus->tgl_lahir ?></td>
			</tr>
				<tr>
					<td align="left">Sekolah Asal</td>
					<td align="left">: SMK Sukabumi - <?php echo $data_lulus->jurusan ?></td>
				</tr>
				<tr>
					<td align="left">Nomor Induk Sekolah</td>
					<td align="left">: <?php echo $data_lulus->nis ?></td>
				</tr>
				<tr>
					<td align="left">Nomor Induk Sekolah Nasional</td>
					<td align="left">: <?php echo $data_lulus->nisn ?></td>
				</tr>
				<tr>
					<td align="left">Nomor Peserta UN</td>
					<td align="left">: <?php echo $data_lulus->no_pesertaun ?></td>
				</tr>
				</table>
				<p>
					<center>Dinyatakan</center>
				</p>
				<h2>
					<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'><rect x='32' y='96' width='64' height='368' rx='16' ry='16' style='fill:none;stroke:#000;stroke-linejoin:round;stroke-width:32px'/><line x1='112' y1='224' x2='240' y2='224' style='fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='112' y1='400' x2='240' y2='400' style='fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><rect x='112' y='160' width='128' height='304' rx='16' ry='16' style='fill:none;stroke:#000;stroke-linejoin:round;stroke-width:32px'/><rect x='256' y='48' width='96' height='416' rx='16' ry='16' style='fill:none;stroke:#000;stroke-linejoin:round;stroke-width:32px'/><path d='M422.46,96.11l-40.4,4.25c-11.12,1.17-19.18,11.57-17.93,23.1l34.92,321.59c1.26,11.53,11.37,20,22.49,18.84l40.4-4.25c11.12-1.17,19.18-11.57,17.93-23.1L445,115C443.69,103.42,433.58,94.94,422.46,96.11Z' style='fill:none;stroke:#000;stroke-linejoin:round;stroke-width:32px'/></svg>
					<?php echo ($data_lulus->status == "LULUS") ? "<font color='green'><b>LULUS</b></font> / <s>TIDAK LULUS</s>" : "<s>LULUS</s> / <font color='red'><b>TIDAK LULUS</b></font>" ; ?>
				</h2>


			</section>

			<p align="center">
				<a href="<?php echo base_url('Pdf/print') ?>/<?php echo $data_lulus->nis ?>/<?php echo $data_lulus->tgl_lahir ?>" class="btn btn-primary" target="_blank">PRINT BUKTI KELULUSAN</a>
			</p>
		</div>

		<!-- FOOTER: DEBUG INFO + COPYRIGHTS -->

		<footer>
			<div class="environment">
				<p>contact : <a href="mailto:bisnis@sukabumiwebsolution.com" style="text-decoration: none; color: white;"><b>bisnis@sukabumiwebsolution.com</b></a></p>
			</div>

			<div class="copyrights">

				<p>&copy; <?php echo (date('Y') == "2020") ? date('Y') : "2020 - ".date('Y') ; ?> SukabumiWebSolution.com & CodeIgniter <?php echo CodeIgniter\CodeIgniter::CI_VERSION ?></p>

			</div>

		</footer>

		<!-- SCRIPTS -->

		<script>
			function toggleMenu() {
				var menuItems = document.getElementsByClassName('menu-item');
				for (var i = 0; i < menuItems.length; i++) {
					var menuItem = menuItems[i];
					menuItem.classList.toggle("hidden");
				}
			}
		</script>
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<!-- -->

	</body>
	</html>
