<!DOCTYPE html>
<html>
<head>
	<title>Print</title>
</head>
<body>
	<table align="center" width="90%">
		<tr>
			<td colspan="2"><center><img src="<?= base_url('public/asset/splash.png') ?>"></center></td>
		</tr>
		<tr>
			<td colspan="2"><center><b>SURAT KETERANGAN LULUS</b></center></td>
		</tr>
		<tr>
			<td colspan="2"><center><b>Nomor : <?= $data_lulus->no_surat ?></b></center></td>
		</tr>
		<tr>
			<td><br/></td>
		</tr>
		<tr>
			<td><br/></td>
		</tr>
		<tr>
			<td colspan="2">Atas nama SMK Sukabumi :</td>
		</tr>
		<tr>
			<td colspan="2"><br/></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>: <b>Subagyo,M.Pd</b></td>
		</tr>
		<tr>
			<td>Jabatan</td>
			<td>: Kepala Sekolah SMK Sukabumi</td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td>: Jl. Salabintana no 999</td>
		</tr>
		<tr>
			<td colspan="2"><br/></td>
		</tr>
		<tr>
			<td colspan="2">Dengan ini menerangkan bahwa :</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>: <?= $data_lulus->nama ?></td>
		</tr>
		<tr>
			<td>Tempat, Tanggal Lahir</td>
			<td>: <?= $data_lulus->tempat_lahir.", ".$data_lulus->tgl_lahir ?></td>
		</tr>
		<tr>
			<td>Sekolah Asal</td>
			<td>: SMK Sukabumi - <?= $data_lulus->jurusan ?></td>
		</tr>
		<tr>
			<td>Nomor Induk Sekolah</td>
			<td>: <?= $data_lulus->nis ?></td>
		</tr>
		<tr>
			<td>Nomor Induk Sekolah Nasional</td>
			<td>: <?= $data_lulus->nisn ?></td>
		</tr>
		<tr>
			<td>Nomor Peserta UN</td>
			<td>: <?= $data_lulus->no_pesertaun ?></td>
		</tr>
		<tr>
			<td colspan="2"><br/></td>
		</tr>
		<tr>
			<td colspan="2">Telah menyelesaikan pendidikan di SMK Sukabumi pada tahun ajaran <?= $data_lulus->thn_ajar ?>, dan berdasalkan hasil penilaian dan kriteria dari sekolah bahwa yang tersebut diatas dinyatakan.</td>
		</tr>
		<tr>
			<td colspan="2"><br/></td>
		</tr>
		<tr>
			<td colspan="2"><center><?= ($data_lulus->status == "LULUS") ? "<font><b>LULUS<b></font> / <s>TIDAK LULUS</s>" : "<s>LULUS</s> / <font><b>TIDAK LULUS<b></font>" ; ?></center></td>
			</tr>
			<tr>
				<td colspan="2"><br/></td>
			</tr>
			<tr>
				<td colspan="2">Demikian surat keterangan ini dibuat dengan sebenarnya dan agar dipergunakan sebagaimana mestinya.</td>
			</tr>
			<tr>
				<td colspan="2">
					<center>
						<img src="https://api.qrserver.com/v1/create-qr-code/?size=50x50&data=<?= $qrcode ?>"><br/>
						<?= $code ?>
						<br/>
						<em>Dokumen ini hasil cetakan komputer dan sah serta tidak memerlukan tandatangan basah.</em>
					</center>
				</td>
			</tr>
		</table>
	</body>
	</html>