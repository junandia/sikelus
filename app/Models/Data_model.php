<?php namespace app\Models;
use CodeIgniter\Model;

class Data_model extends Model
{
	protected $table = 'siswa';

	public function cekLulus($nis = false, $tgl_lahir = false){
		if ($nis == false || $tgl_lahir == false) {
			return "DATA SISWA TIDAK DITEMUKAN";
		}else{
			return $this->getWhere(['nis' => $nis, 'tgl_lahir' => $tgl_lahir]);
		}
	}

	public function cekSiswa($nis = false, $tgl_lahir = false){
		if ($nis == false || $tgl_lahir == false) {
			return "DATA SISWA TIDAK DITEMUKAN";
		}else{
			$this->where('nis', $nis);
			$this->where('tgl_lahir', $tgl_lahir);
			
			return $this->countAllResults();
		}
	}
}

?>