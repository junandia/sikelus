<?php namespace App\Controllers;

use App\Models\Data_model;

class Validasi extends BaseController
{
	public function index($id = ''){
		if ($this->request->getPost('resi')) {
			$id = $this->request->getPost('resi');
		}elseif (!empty($id) || $id != '') {
			$id = $id;
		}else{
			return view('errors/html/error_404');
		}

		$pecah 		= explode('&', base64_decode($id));
		$nis		= $pecah[0];
		$tgl_lahir	= $pecah[1];

		$data['title'] = "Sikelus V.1 - Sistem Informasi Kelulusan";
		$data['resi']  = $id;

		$model = new Data_model();
		if ($model->cekSiswa($nis, $tgl_lahir) > 0) {
			$data['data_lulus'] = $model->cekLulus($nis, $tgl_lahir)->getRow();
			$data['code'] = base64_encode($nis."&".$tgl_lahir);
			$data['qrcode'] = base_url('Validasi/')."/index/".$data['code'];
			$data['yesno'] = "BETUL";
		}else{
			$data['yesno'] = "TIDAK";
		}
		return view('validasi', $data);
	}
}