<?php namespace App\Controllers;

use App\Models\Data_model;

class Home extends BaseController
{
	public function __construct()
	{
		helper('form');
	}

	public function index()
	{
		$data['title'] = "Sikelus V.1 - Sistem Informasi Kelulusan";
		$data['nis'] = [
			'name'      => 'nis',
			'type'		=> 'text',
			'id'        => 'nis',
			'placeholder'     => 'Masukan NIS kamu disini ya',
			'maxlength' => '10',
			'class'		=> 'form-control'
		];
		$data['tgl_lahir'] = [
			'name'      => 'tgl_lahir',
			'type'		=> 'date',
			'id'        => 'tgl_lahir',
			'maxlength' => '10',
			'class'		=> 'form-control'
		];

		if ($this->request->getPost('cek')) {
			$validasi = $this->validate([
				'nis' => 'required|is_natural',
				'tgl_lahir' => 'required|valid_date'
			]);

			if (!$validasi) {
				echo view('welcome_message', $data, [
					'validation' => $this->validator
				]);
			}else{
				$nis 		= $this->request->getPost('nis');
				$tgl_lahir 	= $this->request->getPost('tgl_lahir');

				$model = new Data_model();

				if ($model->cekSiswa($nis, $tgl_lahir) > 0) {
					$data['data_lulus'] = $model->cekLulus($nis, $tgl_lahir)->getRow();
					return view('cek_lulus', $data);
				}else{
					echo "<script>alert('Data Siswa Tidak Ditemukan!')</script>";
					echo "<script>window.location='".base_url()."'</script>";
				}
			}
		}else{
			return view('welcome_message', $data);
		}
	}

	public function validasi(){
		$data['title'] = "Sikelus V.1 - Sistem Informasi Kelulusan";
		$data['resi'] = [
			'name'      => 'resi',
			'type'		=> 'text',
			'id'        => 'resi',
			'placeholder'     => 'Masukan Resi yang terletak dibawah barcode / scan barcode tersebut dan masuk ke link yang muncul.',
			//'maxlength' => '10',
			'class'		=> 'form-control'
		];
		return view('form_validasi', $data);
	}
	//--------------------------------------------------------------------

}
