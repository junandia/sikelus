<?php namespace App\Controllers;

use App\Models\Data_model;
use \Mpdf\Mpdf;

class Pdf extends BaseController
{
	public function print($nis,$tgl_lahir){
		helper('qrcode');
		$view = \Config\Services::renderer();
		//$data['title'] = "Sikelus V.1 - Sistem Informasi Kelulusan";
		$model = new Data_model();

		if ($model->cekSiswa($nis, $tgl_lahir) > 0) {
			$mpdf  = new Mpdf(['mode' => 'utf-8']);
			$data_lulus = $model->cekLulus($nis, $tgl_lahir)->getRow();
			$code = base64_encode($nis."&".$tgl_lahir);
			$qrcode = base_url('Validasi/')."/index/".$code;
			
			$html = $view->setVar('data_lulus', $data_lulus)
			->setVar('code', $code)
			->setVar('qrcode', $qrcode)
			->render('print');
			$mpdf->WriteHTML($html);
			return redirect()->to($mpdf->Output('filename.pdf', 'I'));
		}else{
			echo "<script>alert('Data Siswa Tidak Ditemukan!')</script>";
			echo "<script>window.location='".base_url()."'</script>";
		}
		//return view('print', $data);
	}
}