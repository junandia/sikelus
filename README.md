# SIKELUS v.1

## Apa itu SIKELUS v.1?

SIKELUS atau biasa disebut SISTEM INFORMASI KELULUSAN SISWA adalah informasi untuk pengumuman kelulusan siswa, untuk demo tersedia di [DEMO SIKELUS v.1](https://sikelus.ngundang.my.id/).


## Server Requirements

PHP version 7.2 or higher is required, with the following extensions installed: 

- [intl](http://php.net/manual/en/intl.requirements.php)
- [libcurl](http://php.net/manual/en/curl.requirements.php) if you plan to use the HTTP\CURLRequest library

Additionally, make sure that the following extensions are enabled in your PHP:

- json (enabled by default - don't turn it off)
- [mbstring](http://php.net/manual/en/mbstring.installation.php)
- [mysqlnd](http://php.net/manual/en/mysqlnd.install.php)
- xml (enabled by default - don't turn it off)
